# Teryterator PHP Library

## Install

### Composer

1. Add to composer.json file repository:

```json
    "repositories": [
        {
            [...]
            "type": "git",
            "url": "https://gitlab.com/digitalsnuff/php-teryterator.git"
        }
    ],
```

2. Add line with this library to `require` section object:

```json
    "require": {
        [...]
        "digitalsnuff/php-teryterator": "^0.1.0|^1.0.0"
    }
```

## License

The Teryterator PHP Library is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
