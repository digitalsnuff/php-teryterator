/* 
-- Create database
CREATE DATABASE teryt;

-- Create users
CREATE USER teryt_user WITH INHERIT LOGIN ENCRYPTED PASSWORD '12345';


-- Drop schema
DROP SCHEMA IF EXISTS teryt CASCADE;

-- Create Schema
CREATE SCHEMA IF NOT EXISTS teryt;
SET search_path TO teryt;


-- queries
SELECT rodz, nazwa_dod FROM teryt.terc GROUP BY rodz, nazwa_dod;

-- Constraints
ALTER TABLE teryt.terc ADD CONSTRAINT woj_pow_gmi_rodz_ukey UNIQUE (woj,pow,gmi,rodz); 
ALTER TABLE teryt.simc ADD CONSTRAINT terc_fk FOREIGN KEY (woj,pow,gmi,rodz_gmi) REFERENCES teryt.terc (woj,pow,gmi,rodz);
ALTER TABLE teryt.wmrodz ADD CONSTRAINT rm_ukey UNIQUE (rm);
ALTER TABLE teryt.simc ADD CONSTRAINT wmrodz_rm_fk FOREIGN KEY (rm) REFERENCES teryt.wmrodz (rm);
ALTER TABLE teryt.simc ADD CONSTRAINT sym_ukey UNIQUE (sym);

ALTER TABLE teryt.simc ALTER COLUMN woj SET NOT NULL;
ALTER TABLE teryt.simc ALTER COLUMN pow SET NOT NULL;
ALTER TABLE teryt.simc ALTER COLUMN gmi SET NOT NULL;
ALTER TABLE teryt.simc ALTER COLUMN rodz_gmi SET NOT NULL;
ALTER TABLE teryt.simc ALTER COLUMN rm SET NOT NULL;
ALTER TABLE teryt.simc ALTER COLUMN mz SET NOT NULL;
ALTER TABLE teryt.simc ALTER COLUMN nazwa SET NOT NULL;
ALTER TABLE teryt.simc ALTER COLUMN sym SET NOT NULL;
ALTER TABLE teryt.simc ALTER COLUMN sympod SET NOT NULL;
ALTER TABLE teryt.simc ALTER COLUMN stan_na SET NOT NULL;


ALTER TABLE teryt.ulic ADD CONSTRAINT terc_fk FOREIGN KEY (woj,pow,gmi,rodz_gmi) REFERENCES teryt.terc (woj,pow,gmi,rodz);
ALTER TABLE teryt.ulic ADD CONSTRAINT simc_sym_fk FOREIGN KEY (sym) REFERENCES teryt.simc (sym);
*/


/*
-- PNA
ALTER TABLE pna.pna ADD COLUMN terc_woj text;
ALTER TABLE pna.pna ADD COLUMN terc_pow text;
ALTER TABLE pna.pna ADD COLUMN terc_gmi text;
ALTER TABLE pna.pna ADD COLUMN simc_sym text;
ALTER TABLE pna.pna ADD COLUMN ulic_sym text;

SELECT 
    p.pna,
    p.msc_nazwa pna_msc_nazwa,
    --p.ulica pna_ulica,
    --p.numery pna_numery,
    --t.nazwa terc_nazwa, 
    p.woj_nazwa pna_woj_nazwa,
    p.pow_nazwa pna_pow_nazwa, 
    t.woj terc_woj, 
    p.terc_woj pna_terc_woj,
    t2.pow terc2_pow,
    t2.nazwa terc2_pow_nazwa,
    p.terc_pow pna_terc_pow,
    t3.nazwa terc_gmi_nazwa,
    t3.gmi terc_gmi,
    t3.rodz terc_gmi_rodz,
    t3.nazwa_dod terc_gmi_rodz_nazwa
FROM pna.pna p 
JOIN teryt.terc t ON LOWER(t.nazwa) = LOWER(p.woj_nazwa)
JOIN teryt.terc t2 ON LOWER(t2.nazwa) = LOWER(p.pow_nazwa)
JOIN teryt.terc t3 ON LOWER(t3.nazwa) = LOWER(p.gmi_nazwa)
WHERE t.pow = ''
AND t2.pow <> '' AND t2.gmi = '';
AND t3.pow <> '' AND t3.gmi <> ''
ORDER BY p.msc_nazwa;


SELECT 
    s.nazwa simc_nazwa, 
    s.sym simc_sym, 
    s.sympod simc_sympod,
    t.woj terc_woj,
    t.nazwa terc_woj_nazwa,
    t2.pow terc_pow,
    t2.nazwa terc_pow_nazwa,
    t3.gmi terc_gmi, 
    t3.nazwa terc_gmi_nazwa, 
    t3.rodz terc_gmi_rodz,    
    t3.nazwa_dod terc_gmi_rodz_nazwa,
    s.rm simc_rm,
    w.nazwa_rm wmrodz_nazwa
FROM teryt.simc s
JOIN teryt.terc t ON t.woj = s.woj
JOIN teryt.terc t2 ON t2.woj = s.woj AND t2.pow = s.pow 
JOIN teryt.terc t3 ON t3.woj = s.woj AND t3.pow = s.pow AND t3.gmi = s.gmi AND t3.rodz = s.rodz_gmi
JOIN teryt.wmrodz w ON w.rm = s.rm
WHERE 
t.pow = ''
AND
(t2.pow <> '' AND t2.gmi = '')
AND
(t3.pow <> '' AND t3.gmi <> '')
;


-- update woj in pna
UPDATE 
    pna.pna p 
SET 
    terc_woj = t.woj
FROM 
    teryt.terc t 
WHERE 
    LOWER(t.nazwa) = LOWER(p.woj_nazwa)
AND
    t.pow = '';


-- update pow in pna


UPDATE 
    pna.pna p
SET
    terc_pow = t.pow
FROM 
    teryt.terc t
WHERE
    LOWER(t.nazwa) = LOWER(p.pow_nazwa)
AND 
    t.woj = p.terc_woj
AND
    (t.pow <> '' AND t.gmi = '');


*/



/* TODO add check constraint to text columns with number-only value */
/* TODO create dict table with list of street names only (create from ulic) */