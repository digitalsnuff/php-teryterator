<?php

namespace Teryterator;

use Teryterator\Tools\XMLConverter;
use Teryterator\File;
use Teryterator\Config;
use Teryterator\XML\Migrate as XMLMigrate;


// TODO separate types set. Parse must be for individual set as type

// Create schema for XML for set type of column in database

class Teryterator
{
    private $mode = '';
    private $type = '';
    private $errors = [];
    private $converter;

    // TODO get args for cli
    // TODO CSV to DB

    // TODO add columns created_at, updated_at

    public function __construct($type = '', $mode = '') 
    {
        set_time_limit(300);
        ini_set('memory_limit', '512M');
        
        $config = new Config();
        $this->config = $config->all();

        $this->path_input = $this->config['basepath'] . $this->config['path']['input'] . "/$type"; 
        $this->path_output = $this->config['basepath'] . $this->config['path']['output'] . "/$type"; 

        $this->setType($type);
        $this->setMode($mode);
    }

    public function setMode($mode = '') 
    {
        $this->mode = $mode;
    }

    public function setType($type = '') 
    {
        $this->type = $type;
    }

    public function run()
    {
        if(!$this->validate()) {
            $this->showErrors();
            return false;
        }

        $file = File::getFirst($this->path_input)[0];
        $this->converter = new XMLConverter($file);

       // echo "to". strtoupper($this->mode);

        $this->{"to". strtoupper($this->mode)}();

    }

    private function toCSV($delimiter = ';')
    {

        if(!is_dir($this->path_output)) {
            mkdir($this->path_output);
        }

        $filename = "/$this->type.csv";
        $file = fopen($this->path_output . $filename, 'w+');

        $header = $this->converter->getFields();

        if(!empty($header)) {
            fputcsv($file, $header, $delimiter);
        }

        $rows = $this->converter->yieldRows();
        foreach($rows as $row) {
            fputcsv($file, $row, $delimiter);
        }
    }

    private function toDB()
    {
        $db =  new Db();
        $db->table($this->type, $this->converter->getFields());

        print 'INSERTING...' . Config::EOL;
        $rows = $this->converter->yieldRows();
        foreach($rows as $row) {
            $db->insert($row);
        }

        print 'INSERTING SUCCESSFUL!!!' . Config::EOL;
        
    }

    public function pna()
    {
        $this->type = 'pna';
        $delimiter = ';';

        $filename = [
            'spispna-cz1.txt',
            'spispna-cz2.txt',
            'spispna-cz3.txt',
            'spispna-cz4.txt',
        ];

        $this->path_input = $this->config['basepath'] . $this->config['path']['input'] . "/$this->type"; 
        $this->path_output = $this->config['basepath'] . $this->config['path']['output'] . "/$this->type"; 
        
        if(!is_dir($this->path_output)) {
            mkdir($this->path_output);
        }

        $filename = "$this->path_input/$filename[0]";
        $file = fopen($filename, 'r');

        var_dump($filename);

        $header = explode($delimiter,fgetcsv($file)[0]);

        $header = array_map(function($value) { 
            if(strtolower($value) === strtolower('MIEJSCOWOŚĆ')) {
                return $value = 'msc_nazwa';
            } else if (strtolower($value) === strtolower('GMINA')) {
                return $value = 'gmi_nazwa';
            } else if (strtolower($value) === strtolower('POWIAT')) {
                return $value = 'pow_nazwa';
            } else if (strtolower($value) === strtolower('WOJEWÓDZTWO')) {
                return $value = 'woj_nazwa';
            }

            return strtolower($value);
        },$header);

        var_dump($header);

        $db =  new Db('pna');
        $db->table($this->type, $header);

        while(($row = fgetcsv($file,1000,$delimiter)) !== false) {
            var_dump($row);
            $db->insert($row);
        }
        


        // for superuser only
        // $db->exec("COPY persons(".implode(',',$header).") FROM '$filename' DELIMITER '$delimiter' CSV HEADER;");
    }

    private function validate()
    {
        empty($this->type) ? $this->errors[] = 'Type is not set' : null;
        empty($this->mode) ? $this->errors[] = 'Mode is not set' : null;
        in_array($this->type, Config::TYPES_AVAILABLE) ?: $this->errors[] = sprintf('Type %s is not supported',$this->type);
        in_array($this->mode, Config::MODES_AVAILABLE) ?: $this->errors[] = sprintf('Mode %s is not supported', $this->mode);

        return empty($this->errors);
    }

    public function showErrors()
    {
        if(empty($this->errors)) {
            return false; 
        } 

        echo Config::EOL;
        foreach($this->errors as $error) {
            echo $error . Config::EOL;
        }
    }

}

