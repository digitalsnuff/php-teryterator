<?php

namespace Teryterator\Tools;

use Teryterator\Config;

class XMLConverter extends \XMLReader
{
    private $file_path = '';
    private $basepath = '';
    private $filename = '';
    private $fields =  [];
    private $rows = [];

    public function __construct($file_path = '', $basepath =  '')
    {
        $this->basepath = !empty($basepath) ? $basepath : getcwd();
        $this->file_path = $file_path;

        if($this->validate())
        {
            $this->setFields();
        }
    }

    public function getFields() : array
    {
        return $this->fields;
    }

    public function setFields()
    {

        $this->open($this->file_path);

        while($this->read()) 
        {
            if($this->nodeType === self::ELEMENT) {
                if($this->depth === 2) {
                    $expand = $this->expand();
                    foreach($expand->childNodes as $node) {
                        if($node instanceof \DOMElement)
                        {
                            $this->fields[] = strtolower($node->nodeName);
                        }
                    }
                    break;
                }
            }
        }
    }

    public function getRows() : array
    {
        return $this->rows;
    }

    public function setRows()
    {
        $this->open($this->file_path);

        while($this->read()) 
        {
            if($this->nodeType === self::ELEMENT) {
                if($this->depth === 2) {
                    $expand = $this->expand();
                    $row = [];
                    foreach($expand->childNodes as $node) {
                        if($node instanceof \DOMElement) {
                            $row[strtolower(trim($node->nodeName))] = empty(trim($node->nodeValue)) ? null : trim($node->nodeValue);
                        }
                    }
                    $this->rows[] = $row;
                }
            }
        }
    }

    public function yieldRows()
    {
        $this->open($this->file_path);

        while($this->read()) 
        {
            if($this->nodeType === self::ELEMENT) {
                if($this->depth === 2) {
                    $expand = $this->expand();
                    $row = [];
                    foreach($expand->childNodes as $node) {
                        if($node instanceof \DOMElement) {
                            $row[strtolower(trim($node->nodeName))] = empty(trim($node->nodeValue)) ? null : trim($node->nodeValue);
                        }
                    }
                    yield $row;
                }
            }
        }
    }

    private function validate() 
    {
        if(is_dir($this->file_path)) {
            throw new \Exception('This is no file.');
            return false;
        }

        return true;
    }

}