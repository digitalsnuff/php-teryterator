<?php

namespace Teryterator;

class Config
{

    private $config = [];

    public function __construct(array $config = []) 
    {

        $this->config['basepath'] = getcwd();
        $this->config['input']['path'] = '/input';
        $this->config['output']['path'] = '/output';

        $this->config['db']['default']['provider'] = 'pgsql';
        $this->config['db']['default']['hostname'] = 'localhost';
        $this->config['db']['default']['port'] = '5432';
        $this->config['db']['default']['dbname'] = '';
        $this->config['db']['default']['user'] = '';
        $this->config['db']['default']['password'] = '';

        array_merge($this->config, $config);
    }

    public function all()
    {
        return $this->config;
    }

    public function get($key) 
    {
        return $this->config[$key];
    }
}