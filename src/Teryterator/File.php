<?php

namespace Teryterator;

class File
{
    public static function getFirst($path, $ext = 'xml') : array
    {
        $scanned_directory = array_diff(scandir($path), array('..', '.'));

        foreach($scanned_directory as $key => $dir) {
            if(is_dir($path.DIRECTORY_SEPARATOR.$dir)) {
                unset($scanned_directory[$key]);           
                continue;     
            }
            $file_ext = pathinfo($path.DIRECTORY_SEPARATOR.$dir, PATHINFO_EXTENSION);

            if($file_ext !== $ext) {
                unset($scanned_directory[$key]);
            }
            $scanned_directory[$key] = $path . DIRECTORY_SEPARATOR. $scanned_directory[$key];
        }
        
        rsort($scanned_directory);

        return empty($scanned_directory) ? [] : [array_shift($scanned_directory)];
    }
}