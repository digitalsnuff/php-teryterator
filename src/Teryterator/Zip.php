<?php

namespace Teryterator;

class Zip
{
    public static function unzip($file)
    {
        // echo dirname($file);
        // return true;
        $zip = new \ZipArchive();
        $result = $zip->open($file);
        if($result !== true) {
            return false;
        }
        $zip->extractTo(dirname($file));
        $zip->close();
        return true;
    }
}