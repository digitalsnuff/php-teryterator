<?php

namespace Teryterator;

use Teryterator\Config;

class Db extends \PDO 
{

    private $table_prefix = 'teryt';
    private $table_name_separator = '_';
    private $table_name = '';
    private $fields = [];

    public function __construct($table_prefix = 'teryt')
    {

        $this->table_prefix = $table_prefix;

        $config = new Config();
        $this->config = $config->get('db')['default'];

        $pdo_conn_string = sprintf("%s:host=%s;port=%d;dbname=%s", 
                $this->config['provider'],
                $this->config['hostname'],
                $this->config['port'],
                $this->config['dbname']
                );

        try {
            parent::__construct($pdo_conn_string, $this->config['user'],  $this->config['password']);
            $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\Exception $e) {
            $e->getMessage();
        }

    }

    public function table($table_name, $fields = [])
    {

        // TODO Table class
        $this->fields = $fields;
        
        if(!empty(trim($this->table_prefix))) {
            $this->table_name = "$this->table_prefix$this->table_name_separator$table_name";
        }
        
        if($this->config['provider'] === 'pgsql') {
            print 'Schema: ' . $this->table_prefix . Config::EOL;
            //$pdo->exec('DROP SCHEMA IF EXISTS teryt CASCADE');
            $this->exec("CREATE SCHEMA IF NOT EXISTS $this->table_prefix");
            $this->exec("SET search_path TO $this->table_prefix");
            $this->table_name = $table_name;
        }

        print 'Table name: ' . $this->table_name . Config::EOL;

        $columnsDefList = [];
        $columnsDefList[] = "id SERIAL PRIMARY KEY";

        foreach($fields as $field) {
            $field = strtolower($field);
            if($field === 'stan_na') {
                $columnsDefList[] = "$field date";
                continue;
            }
            $columnsDefList[] = "$field text";
        }
        

        print 'Drop table if exists: ' . $this->table_name . Config::EOL;
        $this->exec("DROP TABLE IF EXISTS $table_name");

        $sql =  "" .
                "CREATE TABLE $table_name (" .
                implode(', ', $columnsDefList) .
                ")"
                ;
        $this->exec($sql);
                
        print 'Create table query: ' . $sql . Config::EOL;
    }

    // TODO assocc array as argument with fields names as keys
    public function insert($rows = [])
    {
        if(empty($rows)) {
            return false;
        }

        $rows = array_map(function($field) { return $this->quote($field);}, $rows);

        $sql =  "INSERT INTO $this->table_name (" .
                implode(', ', $this->fields) . ') ' .
                " VALUES (" .
                implode(', ', $rows) .
                ")";

        // print 'Insert query: ' . $sql . Config::EOL;                

        $this->exec($sql);
    }
}

