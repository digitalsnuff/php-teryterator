<?php

namespace Teryterator;

use Teryterator\Config;

class Convert 
{

    private $config = [];

    public function __construct() {
        set_time_limit(300);
        $configObj = new Config();
        $this->config['basepath'] = $configObj->get('basepath');
        $this->config['output'] = $configObj->get('output');
        
    }

    public function xmlToCSV($file)
    {

        print '****************************************' . '<br />';
        print 'Convert ' . basename($file) . ' to CSV' . '<br />';
        $data = [];
        $breadcrumb = [];
        $output = '';
        $header = [];
        $is_set_header = false;
        $xmlreader = new \XMLReader();

        $xmlreader->open($file);
       
        libxml_use_internal_errors(true);

        while($xmlreader->read()) {
            if($xmlreader->nodeType == \XMLReader::ELEMENT) {
                switch($xmlreader->depth) {
                    case 0: 
                        $breadcrumb[0] = $xmlreader->name;
                        if($xmlreader->name === '') {
                            continue;
                        }
                        $data[$breadcrumb[0]] = [];
                        break;
                    case 1:
                        $breadcrumb[1] = $xmlreader->name;
                        $data[$breadcrumb[0]][$breadcrumb[1]] = [];

                        if($xmlreader->hasAttributes)  {
                            $attr = $xmlreader;
                            while($attr->moveToNextAttribute()) { 
                                $data[$breadcrumb[0]][$breadcrumb[1]]['attr'][$attr->name] = $attr->value;
                            }
                            $attr->moveToFirstAttribute();
                            $attr->moveToElement();
                        }

                        $output_path = $this->config['basepath'].$this->config['output']['path'] . '/' . strtolower($breadcrumb[0]);

                        if(!is_dir($output_path)) {
                            mkdir($output_path);
                        }

                        $output = $output_path .
                                        '/' .
                                        strtolower($data[$breadcrumb[0]][$breadcrumb[1]]['attr']['name']) . 
                                        '_' .
                                        strtolower($data[$breadcrumb[0]][$breadcrumb[1]]['attr']['date']) . 
                                        '.csv';

                                        // TODO use file_put_contents
                                        $fp = fopen($output , 'w+');                                        
                        break;
                    case 2:
                        $expand = $xmlreader->expand();
                        $header = [];
                        $row = [];
                        $delimiter = ';';

                        foreach($expand->childNodes as $node) {
                            if($node instanceof \DOMElement)
                            {
                                if(!$is_set_header) {
                                    $header[] = strtolower($node->nodeName);
                                }
                                $row[] = trim($node->nodeValue);
                            }
                        }

                        if(!$is_set_header) {
                            fputcsv($fp, $header,$delimiter);
                            $is_set_header = true;
                        }
                        fputcsv($fp, $row, $delimiter);
                        //fclose($fp);
                        break;
                }
            }
        }

        print 'SUCCESSFUL' . '<br />';
        print '****************************************' . '<br />';
        print '<br />';

        $xmlreader->close();

        return true;
    }
}